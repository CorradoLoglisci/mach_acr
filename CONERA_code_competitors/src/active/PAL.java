package active;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.Comparator;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import weka.core.Instance;
import weka.core.Instances;


class DecrescentComparator implements Comparator<ExampleFMeasure>{

	@Override
	public int compare(ExampleFMeasure arg0, ExampleFMeasure arg1) {
		if (arg0.d<=arg1.d)
			return 1;
		return -1;
	}
	
}
public class PAL  {
	
	String trainFile;
	String sampleFile;
	double percActive;
	int q=2;
	
	PAL(String trainFile, String sampleFile, double aPerc)
	{
		this.trainFile=trainFile;
		this.sampleFile=sampleFile;
		this.percActive=aPerc;
	}
	
	
	
	static double variance(double[] predictions){
		
		double var=0;
		double avg=0;
		for(double d:predictions)
			avg+=d;
		avg/=predictions.length;
		for(double d:predictions)
			var+=Math.pow(d-avg, 2);
		var/=predictions.length;
		//var=Math.sqrt(var);
		return var;
	}
	
	
		Set<Integer> determineActiveSet(Data data,int a) throws Exception{
		
		
		// Devo costrire q regressori
		Instances dataset[]=data.regularSampling(q);
		
		Regressor ensemble[]=new M5PReg[q];
		for(int i=0;i<q;i++)
			ensemble[i]=new M5PReg(dataset[i]);
		
		
		Instances unlabeleSet=data.unlabeledToInstances();
		
		List<List<Double>> predictionsEnsemble= new LinkedList<List<Double>>();
		
		
		for(int i=0;i<q;i++){
			predictionsEnsemble.add(ensemble[i].classify(unlabeleSet));
		}
		
		Set<ExampleFMeasure> ts=new TreeSet<ExampleFMeasure>(new DecrescentComparator());
		Iterator<Example> it=data.getUnlabeledSetIterator();
		int i=0;
		while(it.hasNext()){
			Example candidateActive=it.next();
			double predictionsOfCandidate[]=new double [q];
			for(int j=0;j<q;j++)
				predictionsOfCandidate[j]=predictionsEnsemble.get(j).get(i);
			ExampleFMeasure palStat= new ExampleFMeasure(candidateActive.getId(), variance(predictionsOfCandidate));
			ts.add(palStat);
			i++;
		}
		TreeSet<Integer> selectedActive=new TreeSet<Integer>();
		Iterator<ExampleFMeasure> itED=ts.iterator();
		 i=0;
		while(itED.hasNext() && i<a){
			ExampleFMeasure ed=itED.next();
			selectedActive.add(ed.id);
			i++;
		}
		return selectedActive;
	}
	
	
	void compute() throws Exception{

		// train file sample file percActive
		
		
		GregorianCalendar gStart= new GregorianCalendar();
		
		Data data = new Data("Aco2CInput//"+trainFile, "Aco2CInput//"+sampleFile);
		
		
		// determino numero di esempi da spostare
		int a=(int)(percActive*data.size());
		if(a<(percActive*data.size()))
			a=a+1; // arrotondo per eccesso
		
		//
		
		String pathConfig = "Aco2COutput"+"/"+sampleFile+"PAL/";
		
		new File(pathConfig.replace(".", "_")).mkdirs();
		String filename = sampleFile;
		Instances trainingSet=data.activeToInstances();
		Regressor r=new M5PReg(trainingSet);
		
		r.serialize(pathConfig+filename+"D0");
		
		GregorianCalendar gEnd= new GregorianCalendar();
		
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream((pathConfig+filename).replace(".", "_")+"Report.txt"), "utf-8"));
		
		String message="***Iteration "+ "0 Elapsed time:"+ (gEnd.getTimeInMillis()-gStart.getTimeInMillis())+"\n";
				
		System.out.print(message);
		writer.write(message);
	
		
		int i=1;
		while(data.unlabeledSize()!=0){
			gStart= new GregorianCalendar();
			System.out.println("Iteration "+i);
			Set<Integer> newActiveSet=determineActiveSet(data,a);
			data.addToActiveSet(newActiveSet);
			data.removeFromUnlabeledSet(newActiveSet);

			trainingSet=data.activeToInstances();
			r=new M5PReg(trainingSet);
			
			r.serialize(pathConfig+filename+"D"+i);
			gEnd= new GregorianCalendar();
			message="***Iteration "+ i+" Elapsed time:"+ (gEnd.getTimeInMillis()-gStart.getTimeInMillis())+"\n";
			writer.write(message);
			
			i++;
			
		}
		writer.close();
		

	}
	
	public static void main(String args[]) throws Exception
	{
		double percActive=0.0;
		String trainFile="";
		String sampleFile="";
		for(int i=0; i<args.length;i+=2)
		{
			if(args[i].equals("-a"))
				percActive=new Double(args[i+1]);
			if(args[i].equals("-data"))
				trainFile=args[i+1];
			if(args[i].equals("-sample"))
				sampleFile=args[i+1];
		}
	
		
		PAL dal=new PAL(trainFile, sampleFile, percActive);
		dal.compute();
		
		
	}

}
