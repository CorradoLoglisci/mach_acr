package active;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.List;

import weka.core.Instances;




public class PredictorDAL {

	public static void main(String[] args) throws Exception{
		
		String sampleFile=args[0];
		String testArffName=args[1];
		String pathConfig = ("Aco2COutput"+"/"+sampleFile+"DAL/").replace(".", "_");
		String pathTestData = "Aco2CInput/";
		
		
		
		
	
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(pathConfig+"Report.csv"), "utf-8"));
	
		writer.write(
				"Iteration;rmse;\n"						
				);
		
		for(int i=0;i<=Integer.MAX_VALUE;i++){

			String fileName=(pathConfig+sampleFile+"D"+i).replace(".", "_");
			try {
				
				Regressor classifier=null;
				try{
					classifier=new Regressor (Regressor.load(fileName));
				}
				catch(Exception e){
					break;
				}
				String fileTestArffName=pathTestData+testArffName;
				
				Data testSet=new Data(fileTestArffName);
				
				Instances testInstances= testSet.unlabeledToInstances();
				
				List<Double> testC=classifier.classify(testInstances);
				
				// Evaluation phase
				
				RMSE rmse=new RMSE(testSet.getUnlabeledSet(),testC);
				
				double rmseV=rmse.compute();
			
				String message=""+i+";"+ rmseV+";";
						
				System.out.println(message);
				message=message.replace(".",",");
				writer.write(message+"\n");
			} catch (ClassNotFoundException | IOException e) {
				System.out.println(e.toString());
			}
			catch (Exception e) {
				System.out.println(e.toString());
			}
		}
		
		
		writer.close();
	
	}

}
