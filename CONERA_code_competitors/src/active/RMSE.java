package active;

import java.util.Iterator;
import java.util.List;

public class RMSE{
	private List<Example> data; 
	private List<Double> predictions;

	public RMSE(List<Example> data, List<Double> predictions) {
		this.data=data;
		this.predictions=predictions;
	}

	public double compute() {
		// TODO Auto-generated method stub
		double sum=0;
		int ct=0;

		Iterator<Double> yIt=predictions.iterator();
		for(Example e: data)
		{
			
				ct++; //anche nodi attivi partecipano al clacolo dello rmse anche se per loro l'errore � zero
				double real=e.getY();
				double pred=yIt.next();
				sum+=Math.pow(real-pred, 2);
				
			
		}
		return Math.sqrt(sum/ct);
	}

}
