package active;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

import weka.core.Instances;

public class DAL {
	
	String trainFile;
	String sampleFile;
	double percActive;
	
	DAL(String trainFile, String sampleFile, double aPerc)
	{
		this.trainFile=trainFile;
		this.sampleFile=sampleFile;
		this.percActive=aPerc;
	}
	
	Set<Integer> determineActiveSet(Data data,int a){
		
		Set<ExampleFMeasure> ts=new TreeSet<ExampleFMeasure>();
		Iterator<Example> it=data.getUnlabeledSetIterator();
		while(it.hasNext()){
			Example candidateActive=it.next();
			ExampleFMeasure dalStat=data.closestExample(candidateActive);
			ts.add(dalStat);
		}
		TreeSet<Integer> selectedActive=new TreeSet<Integer>();
		Iterator<ExampleFMeasure> itED=ts.iterator();
		int i=0;
		while(itED.hasNext() && i<a){
			ExampleFMeasure ed=itED.next();
			selectedActive.add(ed.id);
			i++;
		}
		return selectedActive;
	}
	
	
	void compute() throws Exception{

		// train file sample file percActive
	
	
		GregorianCalendar gStart= new GregorianCalendar();
		
		Data data = new Data("Aco2CInput//"+trainFile, "Aco2CInput//"+sampleFile);
		
		
		// determino numero di esempi da spostare
		int a=(int)(percActive*data.size());
		if(a<(percActive*data.size()))
			a=a+1; // arrotondo per eccesso
		
		//
		
		String pathConfig = "Aco2COutput"+"/"+sampleFile+"DAL/";
		
		new File(pathConfig.replace(".", "_")).mkdirs();
		String filename = sampleFile;
		Instances trainingSet=data.activeToInstances();
		Regressor r=new M5PReg(trainingSet);
		
		r.serialize(pathConfig+filename+"D0");

		GregorianCalendar gEnd= new GregorianCalendar();
		
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream((pathConfig+filename).replace(".", "_")+"Report.txt"), "utf-8"));
		
		String message="***Iteration "+ "0 Elapsed time:"+ (gEnd.getTimeInMillis()-gStart.getTimeInMillis())+"\n";
				
		System.out.print(message);
		writer.write(message);
				

		int i=1;
		while(data.unlabeledSize()!=0){
			gStart= new GregorianCalendar();
			System.out.println("Iteration "+i);
			Set<Integer> newActiveSet=determineActiveSet(data,a);
			data.addToActiveSet(newActiveSet);
			data.removeFromUnlabeledSet(newActiveSet);

			trainingSet=data.activeToInstances();
			r=new M5PReg(trainingSet);
			
			r.serialize(pathConfig+filename+"D"+i);
			gEnd= new GregorianCalendar();
			message="***Iteration "+ i+" Elapsed time:"+ (gEnd.getTimeInMillis()-gStart.getTimeInMillis())+"\n";
			writer.write(message);
					
			i++;
			
		}
		writer.close();

	}
	
	public static void main(String args[]) throws Exception
	{
		double percActive=0.0;
		String trainFile="";
		String sampleFile="";
		for(int i=0; i<args.length;i+=2)
		{
			if(args[i].equals("-a"))
				percActive=new Double(args[i+1]);
			if(args[i].equals("-data"))
				trainFile=args[i+1];
			if(args[i].equals("-sample"))
				sampleFile=args[i+1];
		}
	
		
		DAL dal=new DAL(trainFile, sampleFile, percActive);
		dal.compute();
		
		
	}
}