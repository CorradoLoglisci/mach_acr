package active;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.management.RuntimeErrorException;


import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;



class Example implements Iterable{
	
//	static List<Object> min;
//	static List<Object> max;
	private List<Object> descriptors;
	private Double y;
	private List<Double> predY=new LinkedList<Double>();
	private int id;
	Example (int id,List<Object> descriptors){
		
		this.setId(id);
		this.descriptors=descriptors;
	/*	if(this.min==null){
			min=new LinkedList<Object>();
			max=new LinkedList<Object>();
			for(Object o:descriptors)
			{
				min.add(o);
				max.add(o);
			}
		}else{
			int i=0;
			for(Object o:descriptors){
				if ((Double)o < (Double)min.get(i) )
					min.set(i, o);
				if ((Double)o > (Double)max.get(i) )
					max.set(i, o);
				i++;
			}
		}*/
		
	}
	
	Example (int id,List<Object> descriptors, double y){
		this(id, descriptors);
		this.setY(y);
	}
	
	double distance (Example e)
	{
		double d=0;
		Iterator eIt=e.iterator();
		//Iterator minIt=min.iterator();
		//Iterator maxIt=max.iterator();
		for(Object o:this){
			Object oE=eIt.next();
			//Object oMin=minIt.next();
			//Object oMax=maxIt.next();
			if(o instanceof Double){
				//d+=Math.pow(((Double)o-(Double)oE)/((Double)oMax-(Double)oMin),2);
				d+=Math.pow(((Double)o-(Double)oE),2);
				
			}
			else 
				if(o.equals(oE))
					d+=0;
				else d+=1;
						
		}
		return Math.sqrt(d/descriptors.size());
	}

	@Override
	public Iterator iterator() {
		// TODO Auto-generated method stub
		return descriptors.iterator();
	}
	public String toString(){
		return ""+getId()+ descriptors+getY();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Double getY() {
		return y;
	}

	public void setY(Double y) {
		this.y = y;
	}
	
	

}


class ExampleFMeasure implements Comparable<ExampleFMeasure>{
	int id;
	double d;
	public ExampleFMeasure(int id, double d) {
		// TODO Auto-generated constructor stub
		this.id=id;
		this.d=d;
				
	}
	@Override
	public int compareTo(ExampleFMeasure o) {
		// TODO Auto-generated method stub
		if(d<=o.d) return -1;
		else return 1;
	}
	
	public String toString(){
		return "id"+id+":"+d;
	}
}


public class Data implements Iterable<Example>{
	private List<Example> activeSet=new ArrayList<Example>();
	private List<Example> unlabeledSet=new ArrayList<Example>();
	private LinkedList<String> attributeTypes=new LinkedList<String>();
	
		private Data(){}
		
Data (String fileName)throws Exception{
			
		//	Example.min=null;
		//	Example.max=null;
			BufferedReader br = new BufferedReader(new FileReader(fileName));
			String line=br.readLine();
			
			while(line.indexOf("@data")==-1){
				if(line.indexOf("@attribute")!=-1)
					if(line.indexOf("numeric")!=-1)
						attributeTypes.add("true");
					else
					{
						String values[]=line.split(" ");
						attributeTypes.add(values[2].replace("}", "").replace("{", ""));
						}
				line=br.readLine();
			}
			line=br.readLine();
				while(line!=null){
					String split[]=line.split(",");
					int id=new Integer(split[0]);
					List descriptors=new LinkedList ();
					for(int i=1;i<=split.length-2;i++){
						if(attributeTypes.get(i).equals("true"))
							descriptors.add(new Double(split[i]));
							else
								descriptors.add(split[i]);
					}
					double y=new Double(split[split.length-1]);
					Example e=new Example(id,descriptors,y);
					
					
						unlabeledSet.add(e);
					
					line=br.readLine();
				}
				br.close();
			}

		
		
		Data (String fileName, String sampleName)throws Exception{
			
		//	Example.min=null;
		//	Example.max=null;
			
			BufferedReader br = new BufferedReader(new FileReader(sampleName));
			String line=br.readLine();
			TreeSet<Integer> activeSetId=new TreeSet<Integer>();
			while (line!=null)
			{
				activeSetId.add(new Integer(line));
				line=br.readLine();
			}
			br.close();
			
			br = new BufferedReader(new FileReader(fileName));
			line=br.readLine();
			
			while(line.indexOf("@data")==-1){
				if(line.indexOf("@attribute")!=-1)
					if(line.indexOf("numeric")!=-1)
						attributeTypes.add("true");
					else{
						String values[]=line.split(" ");
						
						attributeTypes.add(values[2].replace("}", "").replace("{", ""));
					}
				line=br.readLine();
			}
			line=br.readLine();
				while(line!=null){
					String split[]=line.split(",");
					int id=new Integer(split[0]);
					List descriptors=new LinkedList ();
					for(int i=1;i<=split.length-2;i++){
						if(attributeTypes.get(i).equals("true"))
							descriptors.add(new Double(split[i]));
							else
								descriptors.add(split[i]);
					}
					double y=new Double(split[split.length-1]);
					Example e=new Example(id,descriptors,y);
					
					if(activeSetId.contains(id))
							activeSet.add(e);
					else
						unlabeledSet.add(e);
					
					line=br.readLine();
				}
				br.close();
			}

		@Override
		public Iterator<Example> iterator() {
			// TODO Auto-generated method stub
			return activeSet.iterator();
		}
		
		public String toString(){
			String s="";
			for(Example e:this ){
				s+=e+"\n";
			}
			return s;
		}
		
		
		ExampleFMeasure closestExample(Example e){
			int id=e.getId();
			double d=activeSet.get(0).distance(e);
			for(int i=1;i<activeSet.size();i++)
			{
				double currentD=activeSet.get(i).distance(e);
				if(currentD<d)
				{
					d=currentD;
					
						
				}
			}
			return new ExampleFMeasure(id, -d);
		}
		
		void addExample(Example e){
			activeSet.add(e);
		}
		
		void addToActiveSet(Set<Integer> newActiveSet){
			
			for(Example e:unlabeledSet){
				if(newActiveSet.contains(e.getId()))
					activeSet.add(e);
			}
			
			
			
		}
		
		
		
		void removeFromUnlabeledSet(Set<Integer> newActiveSet){

			List<Example> tempUnlabeledSet=new ArrayList<Example>();
			
			for(Example e:unlabeledSet){
				if(newActiveSet.contains(e.getId()))
					continue;
				else
					tempUnlabeledSet.add(e);
			}
			this.unlabeledSet=tempUnlabeledSet;
			
		
		}
		
	
		private FastVector createRegressionHeader() throws
		 RuntimeException{
			//creo  l'header di per l'instanziazione dell'Instances
			FastVector attributes = new FastVector();
		
			for(int i=1;i<attributeTypes.size();i++)//salto id
			{
				
				weka.core.Attribute wa = null;
				if(attributeTypes.get(i).equals("true")) //salto id
					wa = new weka.core.Attribute("A"+i,i-1);
				else {
					//throw new RuntimeException("Gestire caso discreto");
					FastVector fv = new FastVector();
					
					for(String s : attributeTypes.get(i).split(",")){
						fv.addElement(s);
					}
				
					wa = new weka.core.Attribute("A"+i,fv,i-1);
					}
					
					
				
				attributes.addElement(wa);
			
			}
			
			
			return attributes;
		}
		
		Instances activeToInstances(){
			
			FastVector fv = createRegressionHeader();
			Instances trainingSet = new Instances("activeSet",fv,0); //creo il trainingSet vuoto
			trainingSet.setClassIndex(fv.indexOf(fv.lastElement())); //imposto l'attributo da classificare
			
			//aggiungo ogni nodo sample al trainingSet
			Instance instance = null;
			
			for(Example e:activeSet){
					instance = new Instance(fv.size());
					instance.setDataset(trainingSet);					
					int j=0;
					for(Object d:e){
						
						if(attributeTypes.get(j+1).equals("true")) // devo saltare id
							instance.setValue(j,(Double)d);
						else
							instance.setValue(j,(String)d);
						
						j++;
					}
					instance.setValue(j,e.getY());
					trainingSet.add(instance);
				}
	
			return trainingSet;
		}
			
	Instances unlabeledToInstances(){
			
			FastVector fv = createRegressionHeader();
			Instances trainingSet = new Instances("activeSet",fv,0); //creo il trainingSet vuoto
			trainingSet.setClassIndex(fv.indexOf(fv.lastElement())); //imposto l'attributo da classificare
			
			//aggiungo ogni nodo sample al trainingSet
			Instance instance = null;
			
			for(Example e:unlabeledSet){
					instance = new Instance(fv.size());
					instance.setDataset(trainingSet);					
					int j=0;
					for(Object d:e){
						
						
						if(attributeTypes.get(j+1).equals("true")) // devo saltare id
							instance.setValue(j,(Double)d);
						else
							instance.setValue(j,(String)d);
						
						j++;
					}
				//	instance.setValue(j,e.getY());
					trainingSet.add(instance);
				}
	
			return trainingSet;
		}
		
	int size(){
		return activeSet.size()+unlabeledSet.size();
	}
	
	int unlabeledSize(){
		return unlabeledSet.size();
	}
	
	Iterator<Example> getUnlabeledSetIterator(){
		return unlabeledSet.iterator();
	}
	
	List<Example> getUnlabeledSet(){
		return unlabeledSet;
	}
	
	Instances[] regularSampling(int q){
		Instances datasets[]=new Instances[q];
		FastVector fv = createRegressionHeader();
		for(int i=0;i<q;i++){
			datasets[i] = new Instances("activeSet",fv,0); //creo il trainingSet vuoto
			datasets[i].setClassIndex(fv.indexOf(fv.lastElement())); //imposto l'attributo da classificare
		}
		Instances active=activeToInstances();
		for(int i=0;i<active.numInstances();i++){
			Instance inst=active.instance(i);
			datasets[i%q].add(inst);
		}
		System.out.println("Processed " + activeSet.size() + " labeled examples");
		int ct=0;
		for(int i=0;i<q;i++){
			System.out.println("Created " + datasets[i].numInstances() +" examples");
			ct+=datasets[i].numInstances();
		}
		if(ct!= activeSet.size()) throw new RuntimeException("Verifica codice");
		return datasets;
		
	}
	
	
		
}
