package active;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

public class ExtractTestingMetrics{

	private List<Double> extract(int columnNumber, String fileName) throws IOException{
		List<Double> results=new ArrayList<Double>();
		BufferedReader br = new BufferedReader(new FileReader(fileName));
		String line=br.readLine();
		line=br.readLine();
		while(line!=null){
			String s[]=line.split(";");
			double v=new Double(s[columnNumber].replace(",","."));
			results.add(v);
			line=br.readLine();
			
			
		}
		br.close();
		return results;
	}

	
	public static void main(String args[]) throws IOException{

		int trainSize=5;
		int sampleSize=5;
		String expName="";
		
		ExtractTestingMetrics m=new ExtractTestingMetrics();
		String path="Aco2COutput/"; //(args[0]);
		String systems[]=args[0].split("_"); // e.g. DAL_PAL
		trainSize=((new Integer(args[1])));
		sampleSize=(new Integer(args[2]));
		expName=(args[3]);
	//	int a=new Integer(args[5]);
	//	int it=new Integer(args[6]);
		
		String columns[]={"Iteration","rmse"};
	
		for(int column=1;column<=1;column++){
			
			BufferedWriter writerMean = new BufferedWriter(new OutputStreamWriter(new FileOutputStream((path+expName+columns[column].toUpperCase()).replace(".", "_")+"TestMean.csv"), "utf-8"));
			BufferedWriter writerStdev = new BufferedWriter(new OutputStreamWriter(new FileOutputStream((path+expName+columns[column].toUpperCase()).replace(".","_")+"TestStdev.csv"), "utf-8"));
			
			for(String system:systems){
				List<Double> rSystemSum=new ArrayList<Double>();
				List<Double> rSystemSum2=new ArrayList<Double>();
			
				int c=0;
				for(int train=1;train<=trainSize;train++){
					for(int sample=1;sample<=sampleSize;sample++)
					{
							//movies2.arffTrain_1.arffSample3.txtSELFa3_it10
							String fileName=expName+"Train_"+train+".arffSample"+sample+".txt"+system; //+"a"+a+"_it"+it;
							fileName=fileName+"/Report";
							fileName=path+fileName;
							fileName=fileName.replace(".", "_")+".csv";
							List<Double> rSample=m.extract(column, fileName);
							for(int i=0;i<rSample.size();i++){
								try{
									rSystemSum.set(i, rSystemSum.get(i)+rSample.get(i));
									rSystemSum2.set(i, rSystemSum2.get(i)+Math.pow(rSample.get(i),2));
								}
								catch(Exception e){
									rSystemSum.add(i, rSample.get(i));
									rSystemSum2.add(i, Math.pow(rSample.get(i),2));
								}
							}
							c++;
					}
				}
				for(int i=0;i<rSystemSum.size();i++){
					rSystemSum.set(i, rSystemSum.get(i)/c); // media
				}
	
				List<Double> rSystemDev=new ArrayList<Double>();
				for(int i=0;i<rSystemSum.size();i++){
					double stdev=rSystemSum2.get(i)-c*Math.pow(rSystemSum.get(i), 2);
					stdev/=c;
					stdev=Math.sqrt(stdev);
					rSystemDev.add(stdev);
				}
				
				// saving mean stdev per system, column
				writerMean.write(system+"Mean");
				for(double d:rSystemSum)
					writerMean.write((";"+d).replace(".", ","));
				writerMean.write("\n");
				writerStdev.write(system+"StDev");
				for(double d:rSystemDev)
					writerStdev.write((";"+d).replace(".", ","));
				writerStdev.write("\n");
			}
			writerMean.close();
			writerStdev.close();
		}
	}

}
