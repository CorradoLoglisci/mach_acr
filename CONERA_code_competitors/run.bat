rem DAL
java -cp dalpal.jar active.DAL -a 0.1 -data atmospheric_arffTrain_1.arff -sample atmospheric_arffTrain_1_arffSample1.txt
java -cp dalpal.jar active.PredictorDAL atmospheric_arffTrain_1_arffSample1_txt atmospheric_arffTest_1.arff

rem PAL
java -cp dalpal.jar active.PAL -a 0.1 -data atmospheric_arffTrain_1.arff -sample atmospheric_arffTrain_1_arffSample1.txt
java -cp dalpal.jar active.PredictorPAL atmospheric_arffTrain_1_arffSample1_txt atmospheric_arffTest_1.arff

pause
